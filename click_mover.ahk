#Persistent    
#NoTrayIcon
#SingleInstance FORCE                                                                    

global sleep_flag = 0
global check_flag = 0

hotkey, LButton, ^!= ;# Binding Left click to this 

;# To make it more interesting, I will stop it from #
;# screwing them up every 15 seconds for 30 seconds, #
;# so it takes longer to figure it out. #

SetTimer, checkTime, 15000	;#Every 15 seconds#

checkTime:	;#Runs once timer hits#
  if sleep_flag = 0
  {
    sleep_flag = 1
    Return
  }
  Else
  {
    If check_flag = 0
    {
      check_flag = 1
      Return
    }
    Else
    {
      sleep_flag = 0
      check_flag = 0
      Return
    }
  }
Return



^!=:: ;# Left click is routed here # 
  If sleep_flag = 1
  {
    change_hotkey("off")
    MouseClick , Left
    change_hotkey("on")
    Return
  }
	MouseGetPos, mouse_x, mouse_y 
	Random, up_or_down, 1, 4
	Random, rand_mouse_x, 20, 100
	Random, rand_mouse_y, 20, 100
	If up_or_down = 1
	{
		new_mouse_x := mouse_x + rand_mouse_x
		new_mouse_y := mouse_y + rand_mouse_y
	}
	Else If up_or_down = 2
	{
		new_mouse_x := mouse_x - rand_mouse_x
		new_mouse_y := mouse_y - rand_mouse_y
	}
	Else If up_or_down = 3
	{
		new_mouse_x := mouse_x + rand_mouse_x
		new_mouse_y := mouse_y - rand_mouse_y
	}
	Else If up_or_down = 4
	{
		new_mouse_x := mouse_x - rand_mouse_x
		new_mouse_y := mouse_y + rand_mouse_y
	}
  change_hotkey("off")
	MouseClick , Left , %new_mouse_x%, %new_mouse_y%,,0
  change_hotkey("on")
Return

!-::
ExitApp
Return

!0::
ExitApp
Return 

change_hotkey(var)
{
  hotkey, LButton, %var%
}